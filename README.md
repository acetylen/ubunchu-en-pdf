# Ubunchu!
A manga featuring Ubuntu, written and drawn by Hiroshi Seo (Seotch) and published by ASCII MEDIA WORKS Inc.
* Licence: [Creative Commons: Attribution – Non-Commcercial](http://creativecommons.org/licenses/by-nc/3.0/)

## Links
### Official Sites:
* Official Author Website (English): https://seotch.wordpress.com/ubunchu/
* Official Author Website (Japanese): http://www.aerialline.com/comics/ubunchu/

### Translations:
* English (RtL) releases of chapters 1-8: https://gitlab.com/ubunchu-translators/ubunchu/
Licence: ([CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/))
* Chapters 9-14 translations (c) by Tarek Saier: https://gitlab.com/sirtetris/ubunchu-translation/
Licence: ([CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/))
